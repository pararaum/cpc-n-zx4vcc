# CPC, Spectrum, Z80, … Development #

Tools to develop for old Z80 machines and some emulators. Tools are:

 * caprice32
 * crasm
 * idsk
 * pasmo
 * sdcc
 * xcpc
 * z80asm/z80dasm
 * z80-asm, z80-mon
 * zesarux

Emulators may be used by logging in as a user and starting a VNC
server.

```
# Log into the docker session as root
docker run --rm -it -v $PWD:/host -w /host vintagecomputingcarinthia/cpc-n-zx4vcc
# Change user to "emu"
su -l emu
# Start VNC
vncserver -geometry 1400x1000
```

On the host use something like `docker inspect` to get the ip address
and connect with a vnc viewer `vncviewer <ip>:1`. Then even the
emulators with their graphical user interface can be run. Do not
expect a fast screen update, though.

# Links #

## Amstrad CPC ##

 * http://www.cantrell.org.uk/david/tech/cpc/cpc-firmware/
 * https://www.cpcwiki.eu/index.php?title=BIOS_Functions
 * https://www.cpcwiki.eu/index.php?title=BIOS_Function_Summary
 * https://www.cpcwiki.eu/index.php/Video_modes
 * http://www.cpcmania.com/Docs/Programming/Converting_and_displaying_an_image_on_the_screen.htm
 * https://www.cpcwiki.eu/index.php/Gate_Array
 * https://www.cpcwiki.eu/index.php/Locomotive_BASIC
 * https://handwiki.org/wiki/Amstrad_CPC_character_set
 * https://www.grimware.org/doku.php/documentations/software/locomotive.basic/start
 * https://www.octoate.de/2016/12/31/lz48lz49-decompressor-for-the-amstrad-cpc/
 * http://www.cpcalive.com/docs/amstrad_cpc_vectors.html
 * https://github.com/cpcitor/cpc-dev-tool-chain

## Z80 ##

 * https://jnz.dk/z80/ (nice web version of Zilog manual)
 * https://www.chibiakumas.com/z80/index.php
 * https://wiki.specnext.dev/Extended_Z80_instruction_set
 * http://www.z80.info/z80syntx.htm
 * https://clrhome.org/table/
 * http://z80.info/decoding.htm
